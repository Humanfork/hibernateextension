Humanfork: Hibernate Extension
------------------------------

A util project that contains different small extensions for Hibernate.
The main extensions are naming strategies:

- `de.humanfork.persistence.hibernate.ShortCutComponentPathNamingStrategy`
- `de.humanfork.persistence.hibernate.CaseSensitiveAwareImplicitNamingStrategy`
- `de.humanfork.persistence.hibernate.CaseSensitiveAwareImplicitNamingStrategyWithHibernate4KeyGeneration` old strategy that generates constraints names like Hibernate 4

Maven
-----
Available from [Maven Central](https://search.maven.org/search?q=g:de.humanfork.hibernateextension)

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.humanfork.hibernateextension/hibernateextension/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.humanfork.hibernateextension/hibernateextension)

```
<dependency>
    <groupId>de.humanfork.hibernateextension</groupId>
    <artifactId>hibernateextension</artifactId>
    <version>1.3.2</version> <!-- or newer -->
</dependency>
```

Website
-------
- https://www.humanfork.de/projects/hibernateextension/ (for releases)
- https://humanfork.gitlab.io/hibernateextension/index.html (for snapshots)
