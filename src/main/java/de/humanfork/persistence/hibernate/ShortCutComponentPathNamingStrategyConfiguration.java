/*
 * Copyright 2010-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.humanfork.persistence.hibernate;

import de.humanfork.persistence.hibernate.NamingUtil.SingularConverter;
import de.humanfork.persistence.hibernate.NamingUtil.StringContains;

/**
 * Configuration for {@link ShortCutComponentPathNamingStrategy}.
 * 
 * It is intended that a customization of {@link ShortCutComponentPathNamingStrategy} can been done by providing an new
 * implementation of this interface and then provide it via Java ServiceLoader.
 * Therefore the full qualified name of an Implementation of this class that should been used in
 * {@link ShortCutComponentPathNamingStrategy} must named in a file:
 * {@code META-INF/services/de.humanfork.persistence.hibernate.ShortCutComponentPathNamingStrategyConfiguration}.
 * 
 * @author engelmann
 */
public interface ShortCutComponentPathNamingStrategyConfiguration {

    /**
     * Provide an instance of {@link AttributeClassNameCompare} that is used by {@link ShortCutComponentPathNamingStrategy}
     * to compare attribute-names and target-class-names in order to decide which style should been used for fk-column names. 
     *
     * @return the attribute class name compare function
     */
    AttributeClassNameCompare getAttributeClassNameCompareFunction();

    /**
     * Provides an instance of {@link SingularConverter} this is used by {@link ShortCutComponentPathNamingStrategy} to
     * convert attribute-names to singular form.
     *
     * @return the singular converter
     */
    SingularConverter getSingularConverter();
    
    
    /**
     * Name prefix used for all tables (Primary entity tables, join tables and, collection tables).
     * If on prefix should been used then the response must been an empty string.
     * 
     * @return table prefix (never null)
     */
    default String getTablePrefix() {
        return "";
    }
    
    
    @FunctionalInterface
    public interface AttributeClassNameCompare {

        /**
         * Check if one string contains the other string (as substring).
         *
         * The simplest implementation is {@link StringContains#includes(String, String)}.
         *
         * @param attributeName the attribute-name
         * @param targetClassName the target-class-name
         * @return {@link StringContains#A_CONTAINS_B} if attribute name contains target name, {@link StringContains#B_CONTAINS_A} if target class name contains attribute name.
         */
        StringContains compareNames(final String attributeName, final String targetClassName);
        
    }

}
