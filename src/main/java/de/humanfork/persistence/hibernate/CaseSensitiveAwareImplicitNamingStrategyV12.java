/*
 * Copyright 2010-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.humanfork.persistence.hibernate;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitJoinTableNameSource;
import org.hibernate.boot.model.naming.ImplicitNamingStrategy;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;

/**
 * The {@link ImplicitNamingStrategy} with Hibernate 5 key generation and Bug like it was implemented in
 * {@link CaseSensitiveAwareImplicitNamingStrategy} in Version 1.2 of this libary.
 *
 * <p>
 * In version 1.3 a bug was fixed in {@link CaseSensitiveAwareImplicitNamingStrategy}, it was that
 * the m:n table was named {@code snake_case(entiyA + "2" + entityB)} but a 1:n table was still named
 * {@code entiyA + "_" + entityB}.
 * </p>
 * <p>
 * If your table names rely on this old naming scheme and you not want to change it, then use this naming strategy
 * instead of {@link CaseSensitiveAwareImplicitNamingStrategy}.
 * </p>
 *
 * <p>
 * Table get lower case names, with sneak_case_syntax.
 * </p>
 */
public class CaseSensitiveAwareImplicitNamingStrategyV12 extends CaseSensitiveAwareImplicitNamingStrategy {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -276014096784535908L;

    /**
     * This is exactly the same implementation like in
     * {@link ImplicitNamingStrategyJpaCompliantImpl#determineJoinTableName(ImplicitJoinTableNameSource)}.
     */
    @Override
    public Identifier determineJoinTableName(final ImplicitJoinTableNameSource source) {
        // JPA states we should use the following as default:
        //      "The concatenated names of the two associated primary entity tables (owning side
        //      first), separated by an underscore."
        // aka:
        //      {OWNING SIDE PRIMARY TABLE NAME}_{NON-OWNING SIDE PRIMARY TABLE NAME}
        final String name = source.getOwningPhysicalTableName() + '_' + source.getNonOwningPhysicalTableName();

        return toIdentifier(name, source.getBuildingContext());
    }

}
