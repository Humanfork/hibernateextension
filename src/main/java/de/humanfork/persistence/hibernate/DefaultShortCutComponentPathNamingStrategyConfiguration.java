/*
 * Copyright 2010-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.humanfork.persistence.hibernate;

import de.humanfork.persistence.hibernate.NamingUtil.SingularConverter;

/**
 * The {@link ShortCutComponentPathNamingStrategyConfiguration} that is used by {@link ShortCutComponentPathNamingStrategy}
 * when no other configuration is given.
 * 
 * @author engelmann
 */
public class DefaultShortCutComponentPathNamingStrategyConfiguration implements ShortCutComponentPathNamingStrategyConfiguration {

    /** The attribute class name compare. */
    private AttributeClassNameCompare attributeClassNameCompare = new NormalizationBasedAttributeClassNameCompare(
            new NormalizationBasedAttributeClassNameCompare.ReplacementRegexPattern("(?i)BusinessId", "bid"));

    /** The singular converter. */
    private SingularConverter singularConverter = NamingUtil.singularConverter();    

    @Override
    public AttributeClassNameCompare getAttributeClassNameCompareFunction() {
        return this.attributeClassNameCompare;
    }

    @Override
    public SingularConverter getSingularConverter() {
        return this.singularConverter;
    }

}