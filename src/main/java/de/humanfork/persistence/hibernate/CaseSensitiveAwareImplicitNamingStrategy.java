/*
 * Copyright 2010-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.humanfork.persistence.hibernate;

import java.beans.Introspector;

import org.hibernate.HibernateException;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitCollectionTableNameSource;
import org.hibernate.boot.model.naming.ImplicitEntityNameSource;
import org.hibernate.boot.model.naming.ImplicitJoinColumnNameSource;
import org.hibernate.boot.model.naming.ImplicitJoinTableNameSource;
import org.hibernate.boot.model.naming.ImplicitNamingStrategy;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;

/**
 * The {@link ImplicitNamingStrategy} with Hibernate 5 key generation.
 * 
 * <ul>
 *   <li>
 *      Tables names are written in lower case snake case style (https://en.wikipedia.org/wiki/Snake_case).
 *   </li>  
 *   <li>
 *     Join tables (m:n) and (1:n) are named {@code entityA "2" entityB} and the entity name is written in lower case
 *     snake case style.
 *   </li>
 *   <li>
 *     Columns are written just like the fields are written (camel case, start with lower case). 
 *   </li>
 *   <li>
 *      FK Columns get named after the referring (target) entity get the prefix "_fk". (They do not get the name of the
 *      source entity field.) 
 *   </li>
 * </ul>
 * 
 * For more details have a look the the method documentation.
 * 
 * <p>
 * If you want to have Hibernate 4 key generation (for FK, UK, ..) inside of a hibernate 5 project
 * then consider using {@link CaseSensitiveAwareImplicitNamingStrategyWithHibernate4KeyGeneration}.
 * Attention: {@link CaseSensitiveAwareImplicitNamingStrategyWithHibernate4KeyGeneration} is deprecated
 * and will be removed in Version 2.0.0.
 * </p>
 *
 * <p>
 * Since version 1.3 a bug is fixed, before a* the m:n table was named {@code snake_case(entiyA + "2" + entityB)}
 * but a 1:n table was still named {@code entiyA + "_" + entityB}, since 1.3 a 1:n table is also named
 * {@code snake_case(entiyA + "2" + entityB)}. If your table names rely on this old naming scheme and you not want to
 * change it then use {@link CaseSensitiveAwareImplicitNamingStrategyV12}.
 * </p>
 */
public class CaseSensitiveAwareImplicitNamingStrategy extends ImplicitNamingStrategyJpaCompliantImpl {

    /** The Constant serialVersionUID. */
    private final static long serialVersionUID = -720564334630994123L;

    /**
     * Provide the entity table name.
     * 
     * Return the same table name as {@link ImplicitNamingStrategyJpaCompliantImpl} would, but with lower case names in
     * sneak_case_syntax.
     *
     * @param source the entity
     * @return the table identifier
     */
    @Override
    public Identifier determinePrimaryTableName(ImplicitEntityNameSource source) {
        if (source == null) {
            // should never happen, but to be defensive...
            throw new HibernateException("Entity naming information was not provided.");
        }

        String tableName = transformEntityName(source.getEntityNaming());
        if (tableName == null) {
            // todo : add info to error message - but how to know what to write since we failed to interpret the naming source
            throw new HibernateException("Could not determine primary table name for entity");
        }

        String lowerCaseTableName = NamingUtil.transformCamelCaseTo_snake_case(tableName);

        return toIdentifier(lowerCaseTableName, source.getBuildingContext());
    }

    /**
     * Provide the table name for 1:n relation ship tables.
     * 
     * Return the same collection table name as {@link ImplicitNamingStrategyJpaCompliantImpl} would, 
     * but with "2" as separator instead of "_" and lower case table name in sneak_case_syntax.
     * 
     * For example {@code User.securityRoles} become {@code user2security_roles}.
     *
     * @param source the source
     * @return the identifier
     */
    @Override
    public Identifier determineJoinTableName(ImplicitJoinTableNameSource source) {
        final String tableName = source.getOwningPhysicalTableName() + '2' + source.getNonOwningPhysicalTableName();

        return toIdentifier(NamingUtil.transformCamelCaseTo_snake_case(tableName), source.getBuildingContext());
    }

    /**
     * Provide the table name for m:n relation ship tables.
     * 
     * Return the same collection table name as {@link ImplicitNamingStrategyJpaCompliantImpl} would, 
     * but with "2" as separator instead of "_" and lower case table name in sneak_case_syntax.
     * 
     * For example {@code User.securityRoles} become {@code user2security_roles}.
     */
    @Override
    public Identifier determineCollectionTableName(ImplicitCollectionTableNameSource source) {
        String entityName = transformEntityName(source.getOwningEntityNaming());

        String tableName = entityName + '2' + transformAttributePath(source.getOwningAttributePath());

        return toIdentifier(NamingUtil.transformCamelCaseTo_snake_case(tableName), source.getBuildingContext());
    }

    /**
     * Provide the name of fk columns (in normal entity tables as well as join tables).
     * 
     * Determine the column name related to {@link javax.persistence.JoinColumn}.  In
     * {@code hbm.xml} terms, this would be a {@code <key/>} defined for a collection
     * or the column associated with a many-to-one.
     *
     * Some note about decapitalizing: We follow the rule of Java Bean Spec.
     *
     * <h1>Java Bean Spec: Chapter 8.8 Capitalization of inferred names.</h1>
     * <p>
     * When we use design patterns to infer a property or event name, we need to decide what rules to follow for
     * capitalizing the inferred name. If we extract the name from the middle of a normal mixedCase style Java name
     * then the name will, by default, begin with a capital letter.
     *
     * Java programmers are accustomed to having normal identifiers start with lower case letters.
     * Vigorous reviewer input has convinced us that we should follow this same conventional rule
     * for property and event names.
     *
     * Thus when we extract a property or event name from the middle of an existing Java name, we
     * normally convert the first character to lower case. However to support the occasional use of all
     * upper-case names, we check if the first two characters of the name are both upper case and if
     * so leave it alone. So for example,
     * - "FooBah" becomes "fooBah"
     * - "Z" becomes "z"
     * - "URL" becomes "URL"
     * We provide a method Introspector.decapitalize which implements this conversion rule.
     * </p>
     */
    @Override
    public Identifier determineJoinColumnName(ImplicitJoinColumnNameSource source) {
        String entityName = transformEntityName(source.getEntityNaming());

        return toIdentifier(Introspector.decapitalize(entityName) + "_fk", source.getBuildingContext());
    }

}
