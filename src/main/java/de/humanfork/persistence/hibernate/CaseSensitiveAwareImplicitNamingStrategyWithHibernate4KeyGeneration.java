/*
 * Copyright 2010-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.humanfork.persistence.hibernate;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitForeignKeyNameSource;

/**
 * This Naming Strategy is an extension of {@link CaseSensitiveAwareImplicitNamingStrategy} with
 * Foraign-Key-Names like in Hibernate 4.
 * 
 * 
 * @author engelmann
 * @deprecated Deprecated since 1.3.0, will be removed in 2.0.0 without replacement
 */
@Deprecated
public class CaseSensitiveAwareImplicitNamingStrategyWithHibernate4KeyGeneration extends CaseSensitiveAwareImplicitNamingStrategy {

	/** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5127684368787582692L;

    /* (non-Javadoc)
	 * @see org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl#determineForeignKeyName(org.hibernate.boot.model.naming.ImplicitForeignKeyNameSource)
	 */
	@Override
	public Identifier determineForeignKeyName(ImplicitForeignKeyNameSource source) {
		String generateHashedFkName = Hibernate4KeyGeneratorUtil.generateSqlKeyName(
											"FK_", source.getTableName(),
											source.getColumnNames());

		return toIdentifier(generateHashedFkName, source.getBuildingContext());
	}

}
