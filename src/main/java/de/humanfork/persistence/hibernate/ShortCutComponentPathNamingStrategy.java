/*
 * Copyright 2010-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.humanfork.persistence.hibernate;

import java.beans.Introspector;
import java.util.List;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.boot.model.naming.EntityNaming;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitCollectionTableNameSource;
import org.hibernate.boot.model.naming.ImplicitEntityNameSource;
import org.hibernate.boot.model.naming.ImplicitJoinColumnNameSource;
import org.hibernate.boot.model.naming.ImplicitJoinTableNameSource;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;
import org.hibernate.boot.model.source.spi.AttributePath;
import org.hibernate.internal.util.StringHelper;
import org.jboss.logging.Logger;

import de.humanfork.persistence.hibernate.NamingUtil.SingularConverter;
import de.humanfork.persistence.hibernate.NamingUtil.StringContains;

/**
 * Naming strategy that combines a more intelligent variant of
 * {@link org.hibernate.boot.model.naming.ImplicitNamingStrategyComponentPathImpl} with
 * {@link CaseSensitiveAwareImplicitNamingStrategy}.
 *
 * <p>
 * This strategy is intended only for cases where the database model is derived from the entity model, but not the
 * other way around. This is, because this strategy use some sophisticated algorithm to build the column names.
 * This algorithm are a bit weak deterministic (the fk column name contains some heuristic). This mean there result is
 * always the same for same input, but a slightly different input can lead to a different the result.
 * We have check the result for a some large entity models and the result was always very well but sometimes a bit
 * surprising. Therefore check always the builded table model in order to verify that it matches to you DB schema!
 * </p>
 *
 *
 * An ImplicitNamingStrategy implementation which uses full composite paths extracted from {@link AttributePath}, as opposed to
 * just the terminal property part.
 *
 * <ul>
 *   <li>
 *      Tables names are written in lower case snake case style (https://en.wikipedia.org/wiki/Snake_case).
 *   </li>
 *   <li>
 *     Join tables (m:n) and (1:n) are named {@code entityA "2" entityB} and the entity name is written in lower case
 *     snake case style.
 *   </li>
 *   <li>
 *     Columns are written just like the fields are written (camel case, start with lower case).
 *   </li>
 *   <li>
 *     Columns for embedded values are uses full composite paths extracted from {@link AttributePath}, as opposed to
 *     just the terminal property part. For example: Entity {@code Product} has the field {@code owner} of embedded class
 *     {@code Person} with field {@code name} then the Table {@code Product} will have a column {@code ower_name}.
 *   </li>
 <li>
 *      FK Column: {@code <attributePath*>_<blownAttributeName>_<fkPostfix>}
 *      <ul>
 *          <li>
 *              {@code attributePath*} *=without last attribute: the path to the attribute (relevant if the field is
 *              located in an embedded) - does not contain the attribute name
 *          </li>
 *          <li>
 *              {@code blownAttributeName} a combination of attribute name and target entity name to contain as much not
 *              duplicated information as possible and (if the entity named does not indicate that it is a plural, then)
 *              it is transformed in a singular for.
 *              First of all, the {@code blownAttributeName} is s singular form (if the entity is not a plural form).
 *              If the attribute-name "contains" then entity-name, then the attribute name (singular) is used.
 *              If the entity-class-name "contains" the attribute-name, then the decapitalized entity class name is used
 *              (no singular transformation).
 *              Else {@code blownAttributeName = <attributeName*>_<EntityClassName*>}
 *              ({@code attributeName*} is sigular if the entity is not a plural form, {@code EntityClassName*} is not
 *              decapitalized)
 *
 *              The "contains" term mean, that both strings get normalized first and then it is checked that one string
 *              contains the other string (or the other way around). The normalization include a toLower case and a
 *              optional customizable replacement, for example "businessID" to "bid".
 *          </li>
 *          <li>
 *              It is {@code _fk} if the referenced column is "id", {@code _cfk_<compoundKeyAttribute>} if the
 *              referenced column ends with "_cfk", and  {@code _fk_<attribute>} as fallback.
 *          </li>
 *      </ul>
 *      <p>
 *        For example: Entity A with id Column Id. Entity B with 3 one-to-one references to A
 *         <code>
 *         class B {
 *           A a  //column a_fk
 *           A myA  //column myA_fk
 *           A something //column something_A_fk
 *         }
 *         </code>
 *
 *         If the name of the refereed Entity of (A) is longer for example LongA then
 *         <code>
 *         class B {
 *           LongA longA //column longA_fk
 *           LongA a  //column longA_fk
 *           LongA myA  //column myA_LongA_fk
 *         }
 *         </code>
 *      </p>
 *   </li>
 * </ul>
 *
 * <p>
 * This Naming strategy can been customized. For example by different (more) rules for normalization while
 * comparing attribute-names and target-entity-class-names (in order to find out which one to use), or with a better
 * configured black list while transforming plural names in singular names.
 * This can been done by an custom implementation of {@link ShortCutComponentPathNamingStrategyConfiguration}.
 * {@link ShortCutComponentPathNamingStrategy} picks up the {@link ShortCutComponentPathNamingStrategyConfiguration} via
 * {@link ServiceLoader}. So the custom implementation must been registered file
 * {@code META-INF/services/de.humanfork.persistence.hibernate.ShortCutComponentPathNamingStrategyConfiguration}
 * that contains the full qualified name of the custom implmentation.
 * </p>
 *
 * @author Ralph Engelmann
 */
public class ShortCutComponentPathNamingStrategy extends ImplicitNamingStrategyJpaCompliantImpl {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2321516766651390062L;

    /** The Logger for this class. */
    private static final Logger LOG = Logger.getLogger(ShortCutComponentPathNamingStrategy.class);

    /**
     * The configuration for building singular forms and attribute compare.
     */
    private final ShortCutComponentPathNamingStrategyConfiguration configuration;

    public ShortCutComponentPathNamingStrategy(final ShortCutComponentPathNamingStrategyConfiguration configuration) {
        if (configuration == null) {
            throw new HibernateException("ShortCutComponentPathNamingStrategy-Configuration was not provided.");
        }

        this.configuration = configuration;
        LOG.info("shortCutComponentPathNamingStrategy-configuration is " + configuration.getClass().getName());
    }

    /**
     * The configuration is loaded via ServicLoader, if no Service Provider is defined, then the default configuration:
     * {@link DefaultShortCutComponentPathNamingStrategyConfiguration} is used.
     */
    public ShortCutComponentPathNamingStrategy() {
        this(loadConfigurationViaServiceLoader()
                .orElseGet(DefaultShortCutComponentPathNamingStrategyConfiguration::new));
    }

    private static Optional<ShortCutComponentPathNamingStrategyConfiguration> loadConfigurationViaServiceLoader() {
        ServiceLoader<ShortCutComponentPathNamingStrategyConfiguration> serviceLoader = ServiceLoader
                .load(ShortCutComponentPathNamingStrategyConfiguration.class);
        List<ShortCutComponentPathNamingStrategyConfiguration> configurations = StreamSupport
                .stream(serviceLoader.spliterator(), false).collect(Collectors.toList());
        switch (configurations.size()) {
        case 0:
            return Optional.empty();
        case 1:
            return Optional.of(configurations.get(0));
        default:
            throw new RuntimeException(
                    "invalid configuration, 0 or 1 registered service provider implementations for `ShortCutComponentPathNamingStrategyConfiguration` expected, but found "
                            + configurations.size() + " : "
                            + configurations.stream().map(x -> x.getClass().getName()).collect(Collectors.joining()));
        }
    }

    /**
     * Return the same table name as {@link ImplicitNamingStrategyJpaCompliantImpl} would, but with lower case names in
     * sneak_case_syntax.
     */
    @Override
    public Identifier determinePrimaryTableName(final ImplicitEntityNameSource source) {
        if (source == null) {
            // should never happen, but to be defensive...
            throw new HibernateException("Entity naming information was not provided.");
        }

        String tableName = transformEntityName(source.getEntityNaming());
        if (tableName == null) {
            // todo : add info to error message - but how to know what to write since we failed to interpret the naming source
            throw new HibernateException("Could not determine primary table name for entity");
        }

        return toIdentifier(this.configuration.getTablePrefix() + NamingUtil.transformCamelCaseTo_snake_case(tableName),
                source.getBuildingContext());
    }

    /**
     * Return the same collection table name as {@link ImplicitNamingStrategyJpaCompliantImpl} would,
     * but with "2" as sperator instead of "_" and lower case table name in sneak_case_syntax.
     *
     * For example {@code User.securityRoles} become {@code user2security_roles}.
     */
    @Override
    public Identifier determineCollectionTableName(final ImplicitCollectionTableNameSource source) {
        String entityName = transformEntityName(source.getOwningEntityNaming());

        String tableName = entityName + '2' + transformAttributePath(source.getOwningAttributePath());

        return toIdentifier(this.configuration.getTablePrefix() + NamingUtil.transformCamelCaseTo_snake_case(tableName), source.getBuildingContext());
    }

    @Override
    public Identifier determineJoinTableName(final ImplicitJoinTableNameSource source) {
        final String tableName = source.getOwningPhysicalTableName() + '2' + source.getNonOwningPhysicalTableName();

        return toIdentifier(this.configuration.getTablePrefix() + NamingUtil.transformCamelCaseTo_snake_case(tableName), source.getBuildingContext());
    }

    /**
     * The Pattern is:
     * {@code <path>_([<attributeName>]|[decapitalized(<className>)]|[<attributeName>_<className>]|)_<seperator>[_<idColumn>]}
     *
     * In the first part there are three choices:
     * <ul>
     *  <li>
     *      {@code [<attributeName>]} is used if the attribute-name is equals to the class-name or attribute-name contains
     *      the class name
     *  </li>
     *  <li>{@code [decapitalized(<className>)]} is used if the class name contains the attribute name</li>
     *  <li>{@code [<attributeName>_<className>]} else</li>
     * </ul>
     *
     * <ul>
     *   <li>
     *      {@code attributeName} is a sigular form if the class name is not a plural form.
     *   </li>
     *   <li>
     *      {@code <seperator>} is {@code fk} or {@code cfk} (compound forain key). {@code cfk} is used the the refereed
     *      columns name ends with {@code cId} (compound Id)
     *   </li>
     *   <li>
     *      {@code <idColumn>} is the referred column. Id  {@code <idColumn>} is {@code id} then it is omitted. If the
     *      is the refereed column ends with {@code cId}, then then end {@code cId} is omitted.
     *  </li>
     * </ul>
     */
    @Override
    public Identifier determineJoinColumnName(final ImplicitJoinColumnNameSource source) {
        String postFix = buildFkPostfix(source);

        final String name;
        if ((source.getNature() == ImplicitJoinColumnNameSource.Nature.ELEMENT_COLLECTION)
                || (source.getAttributePath() == null)) {

            name = Introspector.decapitalize(transformEntityName(source.getEntityNaming())) + postFix;
        } else {
            String[] segments = splitAttributePath(source.getAttributePath());

            /* The lastSegement string become modified several times in this code and gets then writte back to the segments array */
            String lastSegment = segments[segments.length - 1];

            /* convert last segment to singular form but only if the target entity itself is sigular. */
            SingularConverter singularConverter = this.configuration.getSingularConverter();
            boolean targetIsSingular = !singularConverter.isPlural(source.getEntityNaming().getEntityName());
            if (targetIsSingular) {
                lastSegment = singularConverter.toSingularForm(lastSegment);
            }

            String entityClassName = getUnqualifiedClassName(source.getEntityNaming());
            StringContains attributeContainsEntity = this.configuration.getAttributeClassNameCompareFunction()
                    .compareNames(lastSegment, entityClassName);

            boolean entityNameContainsFieldName = attributeContainsEntity == StringContains.B_CONTAINS_A;
            if (entityNameContainsFieldName) {
                //replace last part with longer name
                lastSegment = Introspector.decapitalize(entityClassName);
            }

            segments[segments.length - 1] = lastSegment;

            boolean insertEntityName = attributeContainsEntity == StringContains.DIVERGENT;
            name = String.join("_", segments) + (insertEntityName ? "_" + entityClassName : "") + postFix;
        }

        return toIdentifier(name, source.getBuildingContext());
    }

    /**
     * Build the postfix for foreign key columns.
     * It is {@code _fk} if the referenced column is "id", {@code _cfk_<compoundKeyAttribute>} if the referenced column ends with "_cfk",
     * and  {@code _fk_<attribute>} as fallback.
     * @param source
     * @return the fk column postfix.
     */
    private String buildFkPostfix(final ImplicitJoinColumnNameSource source) {
        String referencedColumnName = source.getReferencedColumnName().getText();
        boolean referenceColumnNameIsId = referencedColumnName.equalsIgnoreCase("id");
        boolean referenceColumnNameIsCommoundKey = StringUtils.endsWithIgnoreCase(referencedColumnName, "_cid");

        if (referenceColumnNameIsId) {
            return "_fk";
        } else if (referenceColumnNameIsCommoundKey) {
            return "_cfk_" + StringUtils.removeEndIgnoreCase(referencedColumnName, "_cid");
        } else {
            return "_fk_" + referencedColumnName;
        }
    }

    @Override
    protected String transformAttributePath(final AttributePath attributePath) {
        return String.join("_", splitAttributePath(attributePath));
    }

    protected static String[] splitAttributePath(final AttributePath attributePath) {
        return splitAttributePath(attributePath, 0);
    }

    protected static String[] splitAttributePath(final AttributePath attributePath, final int elementCount) {
        final String[] result;

        String property = attributePath.getProperty();
        property = property.replace("<", "");
        property = property.replace(">", "");

        boolean notEmpty = property != "";

        if (attributePath.getParent() != null) {
            result = splitAttributePath(attributePath.getParent(), elementCount + (notEmpty ? 1 : 0));
        } else {
            result = new String[elementCount + (notEmpty ? 1 : 0)];
        }

        if (notEmpty) {
            result[(result.length - 1) - elementCount] = property;
        }

        return result;
    }

    /**
     * Return the (unqualified) class name.
     *
     * @param entityNaming the entity naming
     * @return the unqualified class name
     */
    protected String getUnqualifiedClassName(final EntityNaming entityNaming) {
        return StringHelper.unqualify(entityNaming.getEntityName());
    }
}
