/*
 * Copyright 2010-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.humanfork.persistence.hibernate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

/**
 * Util Class to handle names
 * @author engelmann
 */
public final class NamingUtil {

    /** Util classes need no constructor. */
    private NamingUtil() {
        super();
    }

    /**
     * Insert underscores between a lower and an capital letter and make the string complete lower case (afterwards).
     *
     * "." Get also replaced by an underscore.
     *
     * @param name the string.
     *
     * @return the lowercase name string with underscores.
     */
    public static String transformCamelCaseTo_snake_case(final String name) {

        StringBuilder builder = new StringBuilder(name.replace('.', '_'));
        for (int i = 1; i < (builder.length() - 1); i++) {
            if (Character.isLowerCase(builder.charAt(i - 1)) && Character.isUpperCase(builder.charAt(i))
                    && Character.isLowerCase(builder.charAt(i + 1))) {
                builder.insert(i++, '_');
            }
        }
        return builder.toString().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Determine the case (Upper/Lower/Mixed/NoLetter) of the last {@code count} characters of the given {@code name}.
     *
     * This method is just an convenient entry point for {@link LettersCase#lastChars(String, int)},
     * so see  {@link LettersCase#lastChars(String, int)} for documentation details.
     *
     * @param name the name
     * @param count the count
     * @return the last {@code count}-th letters case
     * @see LettersCase#lastChars(String, int)
     */
    public static LettersCase lastLettersCase(final String name, final int count) {
        return LettersCase.lastChars(name, count);
    }

    /**
     * Enum to express case of letters.
     */
    public enum LettersCase {
        /** All letters upper case. */
        ALL_UPPER,

        /** All letters lower case. */
        ALL_LOWER,

        /** Some letters upper case and some lower case. */
        MIXED,

        /** No letter at all. */
        NO_LETTER;

        /**
         * Determine the case (Upper/Lower/Mixed/NoLetter) of the last {@code count} characters of the given
         * {@code name}.
         *
         * <p>
         * If a character is no letter then it is ignored.
         * If all last {@code count} characters are no letters then the result is {@link LettersCase#NO_LETTER}.
         * </p>
         *
         * Examples:
         * <ul>
         *  <li>{@code LettersCase.lastChars("a", 1)} = {@code LettersCase.ALL_LOWER}</li>
         *  <li>{@code LettersCase.lastChars("A", 1)} = {@code LettersCase.ALL_UPPER}</li>
         *  <li>{@code LettersCase.lastChars("ABCdef", 3)} = {@code LettersCase.ALL_LOWER}</li>
         *  <li>{@code LettersCase.lastChars("abcDEF", 3))} = {@code LettersCase.ALL_UPPER}</li>
         *  <li>{@code LettersCase.lastChars("abcdeF", 2)} = {@code LettersCase.MIXED}</li>
         *  <li>{@code LettersCase.lastChars("abcd2f", 2)} = {@code LettersCase.ALL_LOWER}</li>
         *  <li>{@code LettersCase.lastChars("abcD2F", 2)} = {@code LettersCase.ALL_UPPER}</li>
         *  <li>{@code LettersCase.lastChars("abc123", 3)} = {@code LettersCase.NO_LETTER}</li>
         * </ul>
         *
         * @param name the name
         * @param count the count
         * @return the last {@code count}-th letters case
         */
        public static LettersCase lastChars(final String name, final int count) {
            if (name == null) {
                throw new IllegalArgumentException("parameter `name` must not been null");
            }
            if (count > name.length()) {
                throw new StringIndexOutOfBoundsException("String is shorter than " + count);
            }

            int upper = 0;
            int lower = 0;
            for (int i = name.length() - count; i < name.length(); i++) {
                char c = name.charAt(i);
                if (Character.isLetter(c)) {
                    if (Character.isUpperCase(c)) {
                        upper++;
                    }
                    if (Character.isLowerCase(c)) {
                        lower++;
                    }
                }
            }

            if ((upper > 0) && (lower == 0)) {
                return LettersCase.ALL_UPPER;
            } else if ((upper == 0) && (lower > 0)) {
                return LettersCase.ALL_LOWER;
            } else if ((upper > 0) && (lower > 0)) {
                return LettersCase.MIXED;
            } else {
                //upper = 0 and lower = 0
                return LettersCase.NO_LETTER;
            }
        }
    }

    /**
     * Provide an instance of {@link SingularConverter} with default blacklist.
     *
     * @return the singular converter
     */
    public static SingularConverter singularConverter() {
        return SingularConverter.DEFAULT_SINGULAR_CONVERTER;
    }

    /**
     *  Provide an instance of {@link SingularConverter} with given blacklist.
     *
     * @param blacklist the blacklist
     * @return the singular converter
     */
    public static SingularConverter singularConverter(final Collection<String> blacklist) {
        return new SingularConverter(blacklist);
    }

    /**
     * A Converter that convert plural names into singular names.
     * 
     * This class is immutable.     
     */
    public static class SingularConverter {

        static final SingularConverter DEFAULT_SINGULAR_CONVERTER = new SingularConverter(
                Arrays.asList("bus", "series", "status", "process", "adress", "stress"));

        /** If a word ends with this ending, then it can not translated to an singular form, because it is already a singular. */        
        private final List<String> blacklist;

        SingularConverter(final Collection<String> blacklist) {
            this.blacklist = new ArrayList<String>(blacklist);
        }

        /**
        * Checks if the given name is an English plural form.
        *
        * @param name the name
        * @return true, if is plural
        */
        public boolean isPlural(final String name) {
            return !toSingularForm(name).equals(name);
        }

        /**
         * Transform the name, if it is an plural form, to an English singular form.
         *
         *
         * Rules:
         * <ul>
         *  <li>the ending "S" or "IES" is uppercase, then it is not transformed if the letter before is lowercase</li>
         *  <li>if the name ends with "ies" (like entities) then "ies" become replaced by "y"</li>
         *  <li>if the name ends with "s" (like entities) then "s" become removed</li>
         * </ul>
         *
         *
         * Examples
         * <table>
         *  <caption>examples</caption>
         *  <tr><th>given name</th><th>result</th><th>justification   </th></tr>
         *  <tr><td>users     </td><td>user  </td><td>ends with "s"   </td></tr>
         *  <tr><td>user      </td><td>user  </td><td>-               </td></tr>
         *  <tr><td>entities  </td><td>entity</td><td>ends with "ies" </td></tr>
         *  <tr><td>USERS     </td><td>USER  </td><td>ends with "s"   </td></tr>
         *  <tr><td>ENTITIES  </td><td>ENTITY</td><td>ends with "ies" </td></tr>
         *
         *  <tr><td>entitIES  </td><td>entitIES  </td><td>"IES" is uppercase but the letter before not</td></tr>
         *  <tr><td>userS     </td><td>userS  </td><td>"S" is uppercase but the letter before not</td></tr>
         * </table>
         *
         * @param name the name
         * @return the singular form
         */
        public String toSingularForm(final String name) {
            if (name == null) {
                throw new IllegalArgumentException("parameter `name` must not been null");
            }

            if (isBlacklisted(name, this.blacklist)) {
                return name;
            }

            if (name.endsWith("ies")) {
                if (name.length() > 3) {
                    return name.substring(0, name.length() - 3) + "y";
                } else {
                    return name;
                }
            } else if (name.endsWith("IES")) {
                if ((name.length() > 4) && (lastLettersCase(name, 4) == LettersCase.ALL_UPPER)) {
                    return name.substring(0, name.length() - 3) + "Y";
                } else {
                    return name;
                }
            } else if (name.endsWith("s")) {
                /* the second last character must not be an "s" too */
                if ((name.length() > 1) && (Character.toLowerCase(name.charAt(name.length() - 2)) != 's')) {
                    return name.substring(0, name.length() - 1);
                } else {
                    return name;
                }
            } else if (name.endsWith("S")) {
                /* the second last character must not be an "S", but it must been uppercase */
                if ((name.length() > 1) && (name.charAt(name.length() - 2) != 'S')
                        && (lastLettersCase(name, 2) == LettersCase.ALL_UPPER)) {
                    return name.substring(0, name.length() - 1);
                } else {
                    return name;
                }
            } else {
                return name;
            }
        }

        private static boolean isBlacklisted(final String name, final List<String> blacklist) {
            String lowerName = name.toLowerCase(Locale.ENGLISH);
            for (String blackListedName : blacklist) {
                if (lowerName.endsWith(blackListedName.toLowerCase(Locale.ENGLISH))) {
                    return true;
                }
            }
            return false;
        }
    }

    public static StringContains stringContainsEachOther(final String a, final String b) {
        return StringContains.includes(a, b);
    }

    public enum StringContains {
        /** Both strings are equals. */
        EQUALS,
        /** String a contains b (b is substring of a). */
        A_CONTAINS_B, 
        /** String b contains a (a is substring of b). */
        B_CONTAINS_A,
        /** Both strings are different, a is not substring of b and b is not substring of a. */
        DIVERGENT;

        /**
         *  Check if string a contains b or the other way arround.
         *
         * @param a the a
         * @param b the b
         * @return the string contains
         */
        public static StringContains includes(final String a, final String b) {
            if (a == null) {
                throw new IllegalArgumentException("parameter `a` must not been null");
            }
            if (b == null) {
                throw new IllegalArgumentException("parameter `b` must not been null");
            }

            if (a.equals(b)) {
                return StringContains.EQUALS;
            } else if (a.contains(b)) {
                return StringContains.A_CONTAINS_B;
            } else if (b.contains(a)) {
                return StringContains.B_CONTAINS_A;
            } else {
                return StringContains.DIVERGENT;
            }
        }
    }

}
