/*
 * Copyright 2010-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.humanfork.persistence.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.NamingHelper;

/**
 * This class build key (FK, UK, ...) hashes like Hibernate 4 does.
 * 
 * This is almost the same like in Hiberante 5 but wihtout: 
 * {@code .append( "references`" ).append( referencedTableName ).append( "`" );}
 * 
 * @deprecated Deprecated since 1.3.0, will be removed in 2.0.0 when
 *             {@link CaseSensitiveAwareImplicitNamingStrategyWithHibernate4KeyGeneration} is gone
 */
@Deprecated
public class Hibernate4KeyGeneratorUtil {

	/**
	 * Generate Key names that are equals to old hibernate 4 keys.
	 *
	 * @param prefix the prefix
	 * @param tableName the table name
	 * @param columnNames the column names
	 * @return the string
	 */
	public static String generateSqlKeyName(String prefix, Identifier tableName, Collection<Identifier> columnNames) {
		StringBuilder sb = new StringBuilder().append("table`").append(tableName).append("`");

		List<Identifier> sortedColumnNames = new ArrayList<Identifier>(columnNames);
		Collections.sort(sortedColumnNames, CanonicalNameComparator.INSTANCE);

		for (Identifier columnName : sortedColumnNames) {
			sb.append("column`").append(columnName).append("`");
		}
		return prefix + NamingHelper.INSTANCE.hashedName(sb.toString());
	}

	private static final class CanonicalNameComparator implements Comparator<Identifier> {

		private static CanonicalNameComparator INSTANCE = new CanonicalNameComparator();

		@Override
		public int compare(Identifier o1, Identifier o2) {
			return o1.getCanonicalName().compareTo(o2.getCanonicalName());
		}

	}
}
