/*
 * Copyright 2010-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.humanfork.persistence.hibernate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


import de.humanfork.persistence.hibernate.NamingUtil.StringContains;
import de.humanfork.persistence.hibernate.ShortCutComponentPathNamingStrategyConfiguration.AttributeClassNameCompare;

/**
 * An implementation of {@link AttributeClassNameCompare} that applies some normalization to the strings before comparing them.
 * @author engelmann
 */
public class NormalizationBasedAttributeClassNameCompare implements AttributeClassNameCompare {
    
    /** The normalization rules. */
    private final List<NormalizationRule> normalizationRules;
    
    /**
     * Instantiates a new normalization based attribute class name compare.
     *
     * @param normalizationRules the normalization rules
     */
    public NormalizationBasedAttributeClassNameCompare(List<? extends NormalizationRule> normalizationRules) {       
        this.normalizationRules = new ArrayList<>(normalizationRules);
    }
    
    /**
     * Instantiates a new normalization based attribute class name compare.
     *
     * @param normalizationRules the normalization rules
     */
    public NormalizationBasedAttributeClassNameCompare(NormalizationRule... normalizationRules) {       
        this(Arrays.asList(normalizationRules));
    }

    /**
     * Normalize the names and then compare them.
     *
     * @param attributeName the attribute name
     * @param targetClassName the target class name
     * @return the string contains desciption
     */
    @Override
    public StringContains compareNames(String attributeName, String targetClassName) {
        return NamingUtil.stringContainsEachOther(normalizeName(attributeName), normalizeName(targetClassName));
    }
    
    /**
     * Execute the normalization and convert the name to lower case.
     *
     * @param name the name
     * @return the normalized name
     */
    protected String normalizeName(final String name) {
        return executeNormalizationRules(name).toLowerCase(Locale.ENGLISH);
    }

    private String executeNormalizationRules(final String name) {
        String replacedName = name;
        for (NormalizationRule rule : normalizationRules) {
            replacedName = rule.apply(replacedName);
        }
        return replacedName;
    }

    
    /**
     * A normalization rule.
     */
    public interface NormalizationRule {
        
        /**
         * Apply normalization to the given name.
         *
         * @param name the name
         * @return the normalized name
         */
        public String apply(final String name);
    }
    
    /** A regular expression based string normalization. */
    public static class ReplacementRegexPattern implements NormalizationRule {
        
        /** The matching reg ex pattern. */
        private final String regExPattern;

        /** The replacement of each match. */
        private final String replacement;

        /**
         * Instantiates a new replacement regex pattern.
         *
         * @param regExPattern the reg ex pattern
         * @param replacement the replacement
         */
        public ReplacementRegexPattern(final String regExPattern, final String replacement) {
            this.regExPattern = regExPattern;
            this.replacement = replacement;
        }

        /* (non-Javadoc)
         * @see de.humanfork.persistence.hibernate.NormalizationBasedAttributeClassNameCompare.NormalizationRule#apply(java.lang.String)
         */
        @Override
        public String apply(final String name) {
            return name.replaceAll(this.regExPattern, this.replacement);
        }
    }
    
}
