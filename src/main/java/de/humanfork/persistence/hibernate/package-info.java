/*
 * Copyright 2010-2018 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
/**
 * Hibernate Extension.
 * 
 * This extension provides different {@link org.hibernate.boot.model.naming.ImplicitNamingStrategy}s.
 * 
 * <ul>
 *  <li>{@link de.humanfork.persistence.hibernate.CaseSensitiveAwareImplicitNamingStrategy}</li>
 *  <li>{@link de.humanfork.persistence.hibernate.CaseSensitiveAwareImplicitNamingStrategyV12} for backward compatibility</li>
 *  <li>
 *      {@link de.humanfork.persistence.hibernate.CaseSensitiveAwareImplicitNamingStrategyWithHibernate4KeyGeneration}
 *      old strategy that generates constraints names like Hibernate 4.
 *  </li>
 *  <li>{@link de.humanfork.persistence.hibernate.ShortCutComponentPathNamingStrategy}</li>
 * </ul>
 * 
 */
package de.humanfork.persistence.hibernate;

