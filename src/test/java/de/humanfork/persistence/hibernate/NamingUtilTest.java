package de.humanfork.persistence.hibernate;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import de.humanfork.persistence.hibernate.NamingUtil.LettersCase;
import de.humanfork.persistence.hibernate.NamingUtil.StringContains;

public class NamingUtilTest {

    @Nested
    class TransformCamelCaseTo_snake_caseTest {

        @Test
        public void testSimpleName() {
            assertThat(NamingUtil.transformCamelCaseTo_snake_case("test")).isEqualTo("test");
        }

        @Test
        public void testTwoWords() {
            assertThat(NamingUtil.transformCamelCaseTo_snake_case("testExample")).isEqualTo("test_example");
        }

        @Test
        public void testDotSeparator() {
            assertThat(NamingUtil.transformCamelCaseTo_snake_case("test.example")).isEqualTo("test_example");
        }

        @Test
        public void testDotSeparatorAndUppercase() {
            assertThat(NamingUtil.transformCamelCaseTo_snake_case("test.Example")).isEqualTo("test_example");
        }

        @Test
        public void testUnderscoreSeparator() {
            assertThat(NamingUtil.transformCamelCaseTo_snake_case("test_example")).isEqualTo("test_example");
        }

        @Test
        public void testUnderscoreSeparatorAndUppercase() {
            assertThat(NamingUtil.transformCamelCaseTo_snake_case("test_Example")).isEqualTo("test_example");
        }

        /** Test that numbers are treaded like lowercase characters. */
        @ParameterizedTest
        @ValueSource(strings = { "0", "1begin", "end2", "mid3dle" })
        public void testNumbers(final String text) {
            assertThat(NamingUtil.transformCamelCaseTo_snake_case(text)).isEqualTo(text);
        }

        /** If a String starts with upper case character, then this charcter is converted to lower case. */
        @Test
        public void testUpperCaseStart() {
            assertThat(NamingUtil.transformCamelCaseTo_snake_case("Test")).isEqualTo("test");
        }

        @Test
        public void testUpperCaseStartBeanSpec() {
            assertThat(NamingUtil.transformCamelCaseTo_snake_case("WWW")).isEqualTo("www");
        }

        @Test
        public void testUpperCaseStartBeanSpec2() {
            assertThat(NamingUtil.transformCamelCaseTo_snake_case("Www")).isEqualTo("www");
        }

        @Test
        public void testUpperCaseStartBeanSpec3() {
            assertThat(NamingUtil.transformCamelCaseTo_snake_case("WwW")).isEqualTo("www");
        }

        @Test
        public void testUpperCaseStartWithWord() {
            assertThat(NamingUtil.transformCamelCaseTo_snake_case("WWWexample")).isEqualTo("wwwexample");
        }

    }

    @Nested
    class LastLettersCaseTest {

        @Test
        public void lower() {
            assertThat(NamingUtil.lastLettersCase("a", 1)).isEqualTo(LettersCase.ALL_LOWER);
        }

        @Test
        public void upper() {
            assertThat(NamingUtil.lastLettersCase("A", 1)).isEqualTo(LettersCase.ALL_UPPER);
        }

        @Test
        public void allLower() {
            assertThat(NamingUtil.lastLettersCase("ABCdef", 3)).isEqualTo(LettersCase.ALL_LOWER);
        }

        @Test
        public void allUpper() {
            assertThat(NamingUtil.lastLettersCase("abcDEF", 3)).isEqualTo(LettersCase.ALL_UPPER);
        }

        @Test
        public void mixed() {
            assertThat(NamingUtil.lastLettersCase("abcdeF", 2)).isEqualTo(LettersCase.MIXED);
        }

        @Test
        public void withLowerNumbers() {
            assertThat(NamingUtil.lastLettersCase("abcd2f", 2)).isEqualTo(LettersCase.ALL_LOWER);
        }

        @Test
        public void withUpperNumbers() {
            assertThat(NamingUtil.lastLettersCase("abcD2F", 2)).isEqualTo(LettersCase.ALL_UPPER);
        }

        @Test
        public void onlyNumbers() {
            assertThat(NamingUtil.lastLettersCase("abc123", 3)).isEqualTo(LettersCase.NO_LETTER);
        }

        @Test
        public void toShort() {
            Assertions.assertThrows(StringIndexOutOfBoundsException.class, () -> {
                assertThat(NamingUtil.lastLettersCase("ab", 3));
            });
        }
    }

    @Nested
    class ToSingularFromTest {
        @Test
        public void testSimpleSingular() {
            assertThat(NamingUtil.singularConverter().toSingularForm("user")).isEqualTo("user");
        }
        
        @Test
        public void testSimplePlural() {
            assertThat(NamingUtil.singularConverter().toSingularForm("users")).isEqualTo("user");
        }
        
        @Test
        public void testSimplePluralUpperCase() {
            assertThat(NamingUtil.singularConverter().toSingularForm("USERS")).isEqualTo("USER");
        }
        
        @Test
        public void testPluralMixedCase() {
            assertThat(NamingUtil.singularConverter().toSingularForm("USERs")).isEqualTo("USER");
        }
        
        @Test
        public void testPluralUpperCaseS() {
            assertThat(NamingUtil.singularConverter().toSingularForm("stuffS")).isEqualTo("stuffS");
        }

        @Test
        public void testPluralWithIES() {
            assertThat(NamingUtil.singularConverter().toSingularForm("entities")).isEqualTo("entity");
        }
        
        @Test
        public void testPluralWithIESUpperCase() {
            assertThat(NamingUtil.singularConverter().toSingularForm("ENTITIES")).isEqualTo("ENTITY");
        }

        @Test
        public void testSingleS() {
            assertThat(NamingUtil.singularConverter().toSingularForm("s")).isEqualTo("s");
        }

        @ParameterizedTest
        @ValueSource(strings = { "ss", "sss", "ssss", "SS", "SSS", "SSSS", "sS", "Ss"})
        public void testMultipleS(String name) {
            assertThat(NamingUtil.singularConverter().toSingularForm(name)).isEqualTo(name);
        }

        @Test
        public void testSingleIES() {
            assertThat(NamingUtil.singularConverter().toSingularForm("ies")).isEqualTo("ies");
        }
        
        
        @ParameterizedTest
        @ValueSource(strings = { "status", "STATUS", "myStatus", "series", "process"})
        public void testBlackList(String name) {
            assertThat(NamingUtil.singularConverter().toSingularForm(name)).isEqualTo(name);
        }
                
        public void testProcesses(String name) {
            assertThat(NamingUtil.singularConverter().toSingularForm("processes")).isEqualTo("process");
        }

    }

    @Nested
    class StringContainsTest {

        @Test
        public void testEquals() throws Exception {
            assertThat(NamingUtil.stringContainsEachOther("a", "a")).isEqualTo(StringContains.EQUALS);
        }
        
        @Test
        public void testDifferent() throws Exception {
            assertThat(NamingUtil.stringContainsEachOther("a", "b")).isEqualTo(StringContains.DIVERGENT);
        }
        
        @ParameterizedTest
        @ValueSource(strings = { "Bbegin", "EndB", "middleBinA" })
        public void testAContainsB(String a) throws Exception {
            assertThat(NamingUtil.stringContainsEachOther(a, "B")).isEqualTo(StringContains.A_CONTAINS_B);
        }
        
        @ParameterizedTest
        @ValueSource(strings = { "Abegin", "EndA", "middleAinB" })
        public void testBContainsA(String b) throws Exception {
            assertThat(NamingUtil.stringContainsEachOther("A", b)).isEqualTo(StringContains.B_CONTAINS_A);
        }
                
        @Test
        public void testBothEmpty() throws Exception {
            assertThat(NamingUtil.stringContainsEachOther("", "")).isEqualTo(StringContains.EQUALS);
        }
        
        @Test
        public void testAEmpty() throws Exception {
            assertThat(NamingUtil.stringContainsEachOther("", "b")).isEqualTo(StringContains.B_CONTAINS_A);
        }
        
        @Test
        public void testBEmpty() throws Exception {
            assertThat(NamingUtil.stringContainsEachOther("a", "")).isEqualTo(StringContains.A_CONTAINS_B);
        }

        
    }
    

}
