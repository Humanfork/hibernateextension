package de.humanfork.persistence.hibernate;

import static org.assertj.core.api.Assertions.assertThat;

import org.hibernate.boot.model.source.spi.AttributePath;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class ShortCutComponentPathNamingStrategyTest {

    @Nested
    class SplitAttributePathAttributePathTest {

        @Test
        public void testSimplePath() throws Exception {
            AttributePath attributePath = AttributePath.parse("example");           
            String[] result = ShortCutComponentPathNamingStrategy.splitAttributePath(attributePath);
            
            assertThat(result).isEqualTo(new String[] {"example"});
        }
        
        @Test
        public void testTwoPart() throws Exception {
            AttributePath attributePath = AttributePath.parse("demo.example");
            String[] result = ShortCutComponentPathNamingStrategy.splitAttributePath(attributePath);
            
            assertThat(result).isEqualTo(new String[] {"demo", "example"});
        }
        
        @Test
        public void testThreePart() throws Exception {
            AttributePath attributePath = AttributePath.parse("my.demo.example");
            String[] result = ShortCutComponentPathNamingStrategy.splitAttributePath(attributePath);
            
            assertThat(result).isEqualTo(new String[] {"my", "demo", "example"});
        }
    }

}
