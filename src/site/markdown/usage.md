Usage
=====

The current documentation is within the JavaDoc, mostly at package level.

A util project that contains different small extensions for Hibernate.
The main extensions are naming strategies:

- `de.humanfork.persistence.hibernate.ShortCutComponentPathNamingStrategy`
- `de.humanfork.persistence.hibernate.CaseSensitiveAwareImplicitNamingStrategy`
- `de.humanfork.persistence.hibernate.CaseSensitiveAwareImplicitNamingStrategyWithHibernate4KeyGeneration` old strategy that generates constraints names like Hibernate 4